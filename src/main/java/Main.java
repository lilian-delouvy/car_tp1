public class Main {

    public static void main(String[] args) {
        try{
            Server server = new Server();
            server.run();
        }catch (Exception ex){
            System.out.println("Server error: " + ex.getMessage());
        }
    }

}
