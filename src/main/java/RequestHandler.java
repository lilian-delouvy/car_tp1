import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.Objects;

public class RequestHandler implements Runnable{

    private final Socket commandSocket;
    private String serverBaseDirectory;
    private String serverDirectory;
    private boolean isHandling;
    private final Auth account;
    private boolean isLoggedIn;
    private FileHandler fileHandler;

    public RequestHandler(Socket socket) {
        this.commandSocket = socket;
        this.account = new Auth();
        this.serverBaseDirectory = ".\\src\\main\\resources";
        this.serverDirectory = ".\\src\\main\\resources";
        isHandling = true;
        isLoggedIn = false;
    }

    @Override
    public void run() {
        try{
            sendString(Messages.MSG_220);
            handleRequest();

        }catch (SocketException se){
            System.out.println(Messages.MSG_ABORTED);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void handleRequest() throws Exception{
        BufferedReader buffer = new BufferedReader(new InputStreamReader(commandSocket.getInputStream()));
        while(isHandling && !commandSocket.isClosed()){
            String strRequest = buffer.readLine();
            if(strRequest == null){
                sendString(Messages.MSG_TIMEOUT);
                isHandling = false;
            }else{
                CustomRequest request = new CustomRequest(strRequest);
                RequestCommandsEnum requestType = request.getRequestType();
                if(requestType == null){
                    sendString("Commande: " + strRequest + " non implémentée.\r\n");
                }
                else{
                    switch(request.getRequestType()){
                        case USER:
                            handleUserConnection(request);
                            break;
                        case PASS:
                            handleUserPassword(request);
                            break;
                        case QUIT:
                            handleQuit();
                            break;
                        case PORT:
                            handlePort(request);
                            break;
                        case EPRT:
                            handleEPRT(request);
                            break;
                        case RETR:
                            handleRetr(request);
                            break;
                        case LIST:
                            handleList();
                            break;
                        case STOR:
                            handleStor(request);
                            break;
                        case CWD:
                            handleCWD(request);
                            break;
                        case XMKD:
                            handleXMKD(request);
                            break;
                        default:
                            sendString("Commande: " + strRequest + " non implémentée.\r\n");
                            break;
                    }
                }
            }
        }
        commandSocket.close();
    }

    private void handleUserConnection(CustomRequest request) throws Exception {
        String username = request.getCommandArgument();
        account.setUsername(username);
        if(account.user_is_present()){
            sendString(Messages.MSG_331);
        }
        else
            sendString(Messages.MSG_332);
    }

    private void handleUserPassword(CustomRequest request) throws Exception {
        if(account.login(request.getCommandArgument())){
            isLoggedIn = true;
            checkUserDirectory();
            sendString(Messages.MSG_230);
        }else
            sendString(Messages.MSG_332);
    }

    private void checkUserDirectory() {
        File folder = new File(serverBaseDirectory + "\\" + account.getUsername());
        if(folder.mkdir()){
            System.out.println(Messages.MSG_FOLDER_CREATED);
        }
        serverBaseDirectory = folder.getPath();
        serverDirectory = serverBaseDirectory;
    }

    private void handleQuit() throws Exception {
        sendString(Messages.MSG_221);
        isHandling = false;
    }

    private void handlePort(CustomRequest request) throws Exception {
        String[] arguments = request.getCommandArgument().split(",");
        if(arguments.length == 6){
            String IPAdress = arguments[0] + "." + arguments[1] + "." + arguments[2] + "." + arguments[3];
            int portAdress = Integer.parseInt(arguments[4]) * 256 + Integer.parseInt(arguments[5]);
            fileHandler = new FileHandler(IPAdress, portAdress);
            sendString("200 Connection established on " + IPAdress + ":" + portAdress + ".\r\n");
        }
        else{
            sendString(Messages.MSG_425);
        }
    }

    private void handleEPRT(CustomRequest request) throws IOException {
        String[] elements = request.getCommandArgument().split("\\|");
        String address = elements[2];
        int port = Integer.parseInt(elements[3]);
        try{
            fileHandler = new FileHandler(address, port);
            sendString("200 Connection established on " + address + ":" + port + ".\r\n");
        }
        catch (Exception ex){
            sendString(Messages.MSG_425);
        }
    }

    private void handleRetr(CustomRequest request) throws Exception {
        if(isLoggedIn){
            File file = new File(serverDirectory + "/" + request.getCommandArgument());
            if(!file.exists()){
                sendString(Messages.MSG_550);
            }else{
                sendString(Messages.MSG_125);
                DataOutputStream outputStream = new DataOutputStream(fileHandler.socketClient.getOutputStream());
                FileInputStream inputStream = new FileInputStream(file);
                byte[] temp = new byte[fileHandler.socketClient.getSendBufferSize()];
                int data = inputStream.read(temp);

                while(data>0){
                    outputStream.write(temp,0,data);
                    data =inputStream.read(temp);
                }
                inputStream.close();
                outputStream.flush();
                outputStream.close();
                fileHandler.socketClient.close();
                sendString(Messages.MSG_226);
            }
        }else{
            sendString(Messages.MSG_530);
        }
    }

    private void handleList() throws Exception {
        if(isLoggedIn){
            File folder = new File(serverDirectory);
            StringBuilder output= new StringBuilder();

            for(File element: Objects.requireNonNull(folder.listFiles())){
                if(element.isDirectory()){
                    output.append("Folder: ").append(element.getName()).append("\r\n");
                }else{
                    output.append("File: ").append(element.getName()).append("\r\n");
                }
            }

            sendString(Messages.MSG_125);

            DataOutputStream outputStream = new DataOutputStream(fileHandler.socketClient.getOutputStream());
            outputStream.writeBytes(output.toString());
            outputStream.flush();
            outputStream.close();

            fileHandler.socketClient.close();

            sendString(Messages.MSG_226);

        }else{
            sendString(Messages.MSG_530);
        }
    }

    private void handleStor(CustomRequest request) throws Exception {
        if(isLoggedIn){
            sendString(Messages.MSG_125);
            InputStream inputStream = fileHandler.socketClient.getInputStream();

            FileOutputStream fileOutputStream = new FileOutputStream(serverDirectory + "\\" + request.getCommandArgument());

            byte[] temp = new byte[fileHandler.socketClient.getReceiveBufferSize()];

            int data;

            while((data = inputStream.read(temp)) != -1){
                fileOutputStream.write(temp, 0, data);
            }

            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            sendString(Messages.MSG_226);

        }else{
            sendString(Messages.MSG_530);
        }
    }

    private void handleCWD(CustomRequest request) throws Exception {
        if(isLoggedIn){
            String argument = request.getCommandArgument();
            if(argument.startsWith("..")){ //go to the parent directory
                if(!serverDirectory.equals(serverBaseDirectory)){
                    String[] tempString = serverDirectory.split("\\\\");
                    StringBuilder output = new StringBuilder();
                    for(int i = 0; i < tempString.length - 1; i++){
                        if(i != (tempString.length - 2))
                            output.append(tempString[i]).append("\\");
                        else
                            output.append(tempString[i]);
                    }
                    serverDirectory = output.toString();
                }
                sendString(Messages.MSG_200);
            }else{  //select a folder in the repository
                File newFolder = new File(serverDirectory + "/" + request.getCommandArgument());
                if(newFolder.exists()){
                    serverDirectory = newFolder.getPath();
                    sendString(Messages.MSG_200);
                }
                else
                    sendString(Messages.MSG_550);
            }
        }else{
            sendString(Messages.MSG_530);
        }
    }

    private void handleXMKD(CustomRequest request) throws Exception {
        if(isLoggedIn){
            File folder = new File(serverDirectory + "\\" + request.getCommandArgument());
            if(folder.mkdir()){
                sendString(Messages.MSG_200);
            }
        }
        else{
            sendString(Messages.MSG_530);
        }
    }

    private void sendString(String s) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(commandSocket.getOutputStream());
        dataOutputStream.writeBytes(s);
        dataOutputStream.flush();
    }
}
