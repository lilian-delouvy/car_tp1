public class Messages {

    public static String MSG_ABORTED = "Connection aborted by user";

    public static String MSG_TIMEOUT = "Timeout Reached.\r\n";

    public static String MSG_FOLDER_CREATED = "First connection of user: new directory created.";

    public static String MSG_125 = "125 transfer starting.\r\n";

    public static String MSG_200 = "200 Command ok.\r\n";

    public static String MSG_220 = "220 Service ready\r\n";

    public static String MSG_221 = "221 Service closing control connection.\r\n";

    public static String MSG_226 = "226 Closing data connection\r\n";

    public static String MSG_230 = "230 User logged in\r\n";

    public static String MSG_331 = "331 User name ok, need password\r\n";

    public static String MSG_332 = "332 Need account for login\r\n";

    public static String MSG_425 = "425 Can't open data connection\r\n";

    public static String MSG_530 = "530 Not logged in.\r\n";

    public static String MSG_550 = "550 No such file or directory.\r\n";

}
