import java.io.IOException;
import java.net.Socket;

public class FileHandler {

    Socket socketClient;

    public FileHandler(String IP, int port){
        try{
            socketClient = new Socket(IP, port);
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
