import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class Auth {

    private String username;

    public Auth(String username){
        this.username=username;
    }

    public Auth() {

    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername(){
        return username;
    }

    public boolean user_is_present(){
        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(new FileReader("src/main/java/userBase.json"));
            JSONObject jsonObject = (JSONObject) obj;
            //loop array
            JSONArray users = (JSONArray) jsonObject.get("users");
            for( Object value : users ) {
                JSONObject jsonObject2 = (JSONObject) value;
                String username= (String) jsonObject2.get("username");
                if(username.equals(this.username)){
                    return true;
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean login(String password){
        JSONParser parser = new JSONParser();

        try{
            Object obj = parser.parse(new FileReader("src/main/java/userBase.json"));
            JSONObject jsonObject = (JSONObject) obj;
            //loop array
            JSONArray users = (JSONArray) jsonObject.get("users");
            for( Object value : users ) {
                JSONObject user = (JSONObject) value;
                String username= (String) user.get("username");
                String passwordFile= (String) user.get("password");
                if(username.equals(this.username) && passwordFile.equals(password)){
                    return true;
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
