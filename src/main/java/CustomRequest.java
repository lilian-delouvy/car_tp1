public class CustomRequest {

    private RequestCommandsEnum requestType;
    private final String commandArgument;

    public RequestCommandsEnum getRequestType() {
        return requestType;
    }

    public String getCommandArgument() {
        return commandArgument;
    }

    public CustomRequest(String strRequest){
        String[] strArray = strRequest.split(" ");
        setRequestType(strArray[0]);
        if(strArray.length > 1){
            this.commandArgument = strArray[1];
        }
        else{
            commandArgument = "NULL";
        }
    }

    private void setRequestType(String type){
        switch (type){
            case "USER":
                requestType = RequestCommandsEnum.USER;
                break;
            case "PASS":
                requestType = RequestCommandsEnum.PASS;
                break;
            case "QUIT":
                requestType = RequestCommandsEnum.QUIT;
                break;
            case "PORT":
                requestType = RequestCommandsEnum.PORT;
                break;
            case "EPRT":
                requestType = RequestCommandsEnum.EPRT;
                break;
            case "RETR":
                requestType = RequestCommandsEnum.RETR;
                break;
            case "LIST":
                requestType = RequestCommandsEnum.LIST;
                break;
            case "STOR":
                requestType = RequestCommandsEnum.STOR;
                break;
            case "CWD":
                requestType = RequestCommandsEnum.CWD;
                break;
            case "XMKD":
                requestType = RequestCommandsEnum.XMKD;
                break;
            default:
                requestType = null;
                break;
        }
    }
}
