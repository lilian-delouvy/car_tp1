import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private boolean isRunning;
    private final ServerSocket socketCommand;

    public void stopServer(){
        this.isRunning = false;
    }

    public Server() throws IOException {
        this.socketCommand = new ServerSocket(8080);
        isRunning = true;
    }

    public void run(){
        try{
            System.out.println("Server started on " + this.socketCommand.getLocalPort());
            while(isRunning){
                Socket socket = this.socketCommand.accept();
                new Thread(new RequestHandler(socket)).start();
            }
        }catch (Exception ex){
            System.out.println("Server error: " + ex.getMessage());
            stopServer();
        }
    }

}
