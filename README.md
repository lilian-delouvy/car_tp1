# TP1 CAR

## Auteurs

Conrad Baudelet

Lilian Delouvy

## Introduction

Ce serveur FTP a été réalisé durant le cours de CAR de Master 1 MIAGE à l'Université de Lille.

## Contenu

Les commandes suivantes sont disponibles:

* USER
* PASS
* QUIT
* PORT
* EPRT
* RETR
* LIST
* STOR
* CWD
* MKDIR

## Contribution

Pour implémenter de nouvelles commandes, veuillez consulter le lien suivant:
[Contribuer](https://docs.google.com/document/d/1KbVqhDPYU2xnqkjBjyYglLipOijJ5QxAusuG8fZcVjE/edit?usp=sharing)
