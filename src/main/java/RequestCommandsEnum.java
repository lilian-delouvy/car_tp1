public enum RequestCommandsEnum {
    USER,
    PASS,
    QUIT,
    PORT,
    EPRT,
    RETR,
    LIST,
    STOR,
    CWD,
    XMKD
}
